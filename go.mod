module gitlab.com/sijisu/rplidar-vis

go 1.16

require (
	github.com/tfriedel6/canvas v0.12.1
	gitlab.com/sijisu/rplidar v1.0.4
)
