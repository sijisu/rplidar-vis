package main

import (
	"fmt"
	"math"

	"github.com/tfriedel6/canvas/sdlcanvas"
	l "gitlab.com/sijisu/rplidar"
)

func main() {
	// we want to use the defaults
	lidar, _ := l.NewRPLidar("", 0, false)
	wnd, cv, err := sdlcanvas.CreateWindow(1440, 900, "Vis")
	if err != nil {
		panic(err)
	}
	defer wnd.Destroy()

	scans, stop := lidar.IterScans(0, 0)

	w, h := float64(cv.Width()), float64(cv.Height())

	mw, mh := math.Round(w/2), math.Round(h/2)

	wnd.MainLoop(func() {
		i := <-scans
		cv.SetFillStyle("#000")
		cv.FillRect(0, 0, w, h)
		cv.SetFillStyle("#FF0000")
		cv.BeginPath()
		cv.Arc(mw, mh, 5, 0, 2*math.Pi, false)
		cv.ClosePath()
		cv.Fill()
		cv.SetFont("Roboto-Light.ttf", 18)
		cv.SetFillStyle("#FFFFFF")
		cv.FillText(fmt.Sprintf("Measurements in scan: %d", len(i)), 10, 30)
		for _, mes := range i {
			cv.SetFillStyle(int(math.Min(255-((mes.Distance/10000)*200), 50)), int(math.Min(350-((mes.Distance/10000)*255), 255)), int(math.Min(((mes.Distance/10000)*200), 255)))
			x := math.Sin(((360-mes.Angle)/360)*2*math.Pi) * mes.Distance
			y := math.Cos(((360-mes.Angle)/360)*2*math.Pi) * mes.Distance
			cv.FillRect(((x/10000)*mw)+mw, ((y/10000)*mh)+mh, 3, 3)
			//fmt.Printf("mes: %t %d %f %f\n", mes.IsNewScan, mes.Quality, mes.Angle, mes.Distance)
		}
	})

	// stop the measuring
	defer close(stop)
}
